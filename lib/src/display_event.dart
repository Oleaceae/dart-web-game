part of game;

class DisplayEvent extends Event {
  List<SpriteObject> spriteObjects = [];
  List<TextObject> textObjects = [];

  DisplayEvent(String eventID, this.textObjects, this.spriteObjects,
      int duration, bool blocking)
      : super(eventID, blocking) {
    this.duration = duration;

    event = (controller) {
      running = true;
      controller.view.renderDisplayEvent(this);
      return blocking;
    };
  }

  DisplayEvent.gameOver(String eventID, PlayerCharacter playerCharacter)
      : super(eventID, true) {
    duration = -1;
    textObjects.add(TextObject(
        'You lose. Restart with F5.',
        -1,
        playerCharacter.position +
            Vector2(-64.0, playerCharacter.size.toDouble()),
        16,
        'white'));
    event = (controller) {
      running = true;
      controller.view.renderDisplayEvent(this);
      return blocking;
    };
  }
}
