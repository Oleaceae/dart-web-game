part of game;

class TextObject extends GenericObject {
  String text = '';
  String color = '';

  TextObject(this.text, int id, Vector2 position, int size, this.color)
      : super(id, position, size);
}
