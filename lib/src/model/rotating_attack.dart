part of game;

class RotatingAttack extends ProjectileAttack {
  double rotation = 0;
  double rotationSpeed = 2.0;
  int rotationArc = 45;
  bool rotateLeft = false;

  RotatingAttack(
      super.dmg, super.cooldownDuration, super.speed, super.size, super.sprite);

  @override
  RotatingAttack.clone(RotatingAttack og, Vector2 direction, Vector2 position)
      : super(og.dmg, og.cooldownDuration, og.speed, og.size, og.sprite,
            og.randomAttackArcDegrees) {
    super.position = position;
    super.direction = direction;
    super.id = getUID();

    rotateLeft = og.rotateLeft;

    if (og.randomAttackArcDegrees != null) {
      double deviation = random.nextInt(og.randomAttackArcDegrees!) -
          og.randomAttackArcDegrees! / 2;
      this.direction = direction.rotateDegrees(deviation);
    }
  }

  @override
  RotatingAttack clone(Vector2 direction, Vector2 position) {
    rotateLeft = !rotateLeft;
    return RotatingAttack.clone(this, direction, position);
  }

  @override
  void update() {
    position = direction.rotateDegrees(rotation) * speed + position;
    if (rotateLeft) {
      rotation = rotation - rotationSpeed;
      if (rotation <= -rotationArc) rotateLeft = false;
    } else {
      rotation = rotation + rotationSpeed;
      if (rotation >= rotationArc) rotateLeft = true;
    }
  }
}
