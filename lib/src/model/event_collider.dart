part of '../../game.dart';

class EventCollider extends Collider {
  Event? event;

  EventCollider(this.event, int id, Vector2 position, int size, Sprite sprite)
      : super(id, position, size, sprite);

  EventCollider.fromCollider(Collider collider)
      : super(collider.id, collider.position, collider.size, collider.sprite);
}
