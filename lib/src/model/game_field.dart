part of game;

// Holds all the objects that make up a single level. Size may be static,
// but the DOM applies a size factor for rendering when window size can't
// display the entire field.
class GameField {
  final double width = 1080;
  final double height = 900;

  String currentLevel = '';

  String backgroundImage = 'assets/background/defaultBackground.png';

  PlayerCharacter pc = PlayerCharacter.simple();

  List<Enemy> enemies = [];
  List<ProjectileAttack> activeAttacks = [];
  List<Collider> colliders = [];
  List<SpriteObject> staticSprites = [];
  List<Exit> exits = [];
  List<Exit> lockedExits = [];
  List<EventCollider> itemPickups = [];
  List<TextObject> textObjects = [];

  List<Event> events = [];

  GameField(
      this.currentLevel,
      this.pc,
      this.enemies,
      this.colliders,
      this.staticSprites,
      this.exits,
      this.lockedExits,
      this.itemPickups,
      this.textObjects,
      this.backgroundImage);

  GameField.clone(GameField og)
      : this(
            og.currentLevel,
            og.pc,
            og.enemies,
            og.colliders,
            og.staticSprites,
            og.exits,
            og.lockedExits,
            og.itemPickups,
            og.textObjects,
            og.backgroundImage);

  GameField.empty();

  Enemy getEnemy(int id) {
    return enemies.firstWhere((other) {
      return other.id == id;
    }, orElse: () => Enemy.error());
  }

  void removeEnemy(Enemy enemy) {
    enemies.removeWhere((other) {
      return other.id == enemy.id;
    });
    if (enemies.isEmpty) return;
    enemies.sort((enemy, other) {
      return enemy.id.compareTo(other.id);
    });
  }
}
