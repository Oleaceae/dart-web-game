part of game;

class Sprite {
  String up = "";
  String down = "";
  String right = "";
  String left = "";
  String current = "";

  Sprite(this.up, this.down, this.right, this.left, this.current);

  Sprite.single(String path) {
    up = path;
    down = path;
    right = path;
    left = path;
    current = path;
  }

  Sprite.fromPath(String path, {String current = "up"}) {
    up = path + 'up.png';
    down = path + 'down.png';
    right = path + 'right.png';
    left = path + 'left.png';
    this.current = path + current + '.png';
  }

  Sprite.none() {
    up = "assets/none/placeholder.png";
    down = "assets/none/placeholder.png";
    right = "assets/none/placeholder.png";
    left = "assets/none/placeholder.png";
    current = "assets/none/placeholder.png";
  }
}
