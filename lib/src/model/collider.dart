part of game;

class Collider extends SpriteObject {
  Collider.noSprite(int id, Vector2 position, int size)
      : super(id, position, size, Sprite.none());

  Collider(int id, Vector2 position, int size, Sprite sprite)
      : super(id, position, size, sprite);

  bool collidesWith(Collider other) =>
      position.dx + size >= other.position.dx &&
      position.dx <= other.position.dx + other.size &&
      position.dy + size >= other.position.dy &&
      position.dy <= other.position.dy + other.size;

  bool outOfBounds(GameField field) =>
      position.dx <= 0 ||
      position.dy <= 0 ||
      position.dx + size >= field.width ||
      position.dy + size >= field.height;
}
