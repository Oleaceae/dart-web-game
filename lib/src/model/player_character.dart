part of game;

enum Direction {
  up,
  down,
  left,
  right,
}

class PlayerCharacter extends Collider {
  int maxHp = 10;
  int currentHp = 10;

  PlayerAttack attack = PlayerAttack(
      1, 20, 2.3, 32, Sprite.fromPath('assets/pc/attack/', current: 'up'));

  List<PlayerAttack> activeAttacks = [];

  Direction direction = Direction.up;

  final int invulnerabilityDuration = 120;
  bool invulnerable = false;

  double moveSpeed = 5.0;

  PlayerCharacter(this.maxHp, this.currentHp, int id, Vector2 position,
      int size, this.moveSpeed, [PlayerAttack? attack])
      : super(
            id,
            position,
            size,
            Sprite(
                'assets/pc/back.png',
                'assets/pc/front.png',
                'assets/pc/right.png',
                'assets/pc/left.png',
                'assets/pc/back.png')) {
    if (attack != null)
      this.attack = attack;
    else
      this.attack = PlayerAttack.simple();
  }

  PlayerCharacter.simple()
      : super(
            -1,
            Vector2(0, 0),
            64,
            Sprite(
                'assets/pc/back.png',
                'assets/pc/front.png',
                'assets/pc/right.png',
                'assets/pc/left.png',
                'assets/pc/back.png'));

  void damaged(int dmg, UIController uiController, DOMManipulator view,
      GameField field) {
    if (currentHp > 0) {
      currentHp -= dmg;
      eventQueue.add(
          Event.invulnerability('playerInvulnerable', invulnerabilityDuration));
    }
    if (currentHp <= 0) {
      currentHp = 0;
      eventQueue.add(DisplayEvent.gameOver('gameOver', this));
    }
    uiController.hearts = uiController.updateHearts(field);
    view.updateHearts(uiController);
  }

  GameField move(Vector2 vec, GameField field, LevelManager levelManager) {
    if (vec.dx == 0 && vec.dy == 0) return field;

    String current = sprite.current;

    if (vec.dx > 0) {
      direction = Direction.right;
      sprite.current = sprite.right;
    } else if (vec.dx < 0) {
      direction = Direction.left;
      sprite.current = sprite.left;
    } else if (vec.dy > 0) {
      direction = Direction.down;
      sprite.current = sprite.down;
    } else if (vec.dy < 0) {
      direction = Direction.up;
      sprite.current = sprite.up;
    }

    if (sprite.current != current) {
      eventQueue.add(Event.updatePlayerSprite('updatePlayer', this));
    }

    if (vec.dx != 0 && vec.dy != 0) {
      vec = vec.normalize();
    }
    Collider projectedLocation =
        Collider.noSprite(0, position + vec * moveSpeed, size);

    for (Exit exit in field.exits) {
      if (projectedLocation.collidesWith(exit)) {
        eventQueue.add(Event.levelChanged(field, exit));
      }
    }

    for (Collider collider in field.colliders + field.lockedExits) {
      if (projectedLocation.collidesWith(collider)) return field;
    }

    for (EventCollider collider in field.itemPickups) {
      if (projectedLocation.collidesWith(collider)) {
        if (collider.event == null) {
          continue;
        }
        // Some events may not be picked up due to unfulfilled conditions
        // (Like already having the maximum HP in case of health recovery)
        // But the events are still put into the queue and processed,
        // leaving the "finished" event in the item.
        // Reset the event to avoid this.
        collider.event!.reset();
        eventQueue.addUnique(collider.event!);
      }
    }

    if (projectedLocation.outOfBounds(field)) return field;

    position = position + vec * moveSpeed;

    return field;
  }

  bool isInvulnerable() {
    return invulnerable;
  }

  bool isFullHp() {
    return currentHp >= maxHp;
  }

  void increaseHp(int amount) {
    currentHp += amount;
    // Clamp to max hp
    if (currentHp > maxHp) {
      currentHp = maxHp;
    }
  }

  void setInvulnerable(bool invulnerable) {
    this.invulnerable = invulnerable;
  }

  void addAttack() {
    Vector2 attackPosition = getAttackPosition(attack);
    attack.cooldown = 0;
    activeAttacks.add(PlayerAttack.clone(attack, direction, attackPosition));
  }

  void updateActiveAttacks() {
    for (PlayerAttack attack in activeAttacks) {
      attack.position = getAttackPosition(attack);
    }
  }

  Vector2 getAttackPosition(PlayerAttack attack) {
    Vector2? attackPosition;
    if (direction == Direction.up) {
      attackPosition = Vector2(position.dx + (size / 2 - attack.size / 2),
          position.dy - attack.size);
      attack.sprite.current = attack.sprite.up;
    } else if (direction == Direction.down) {
      attackPosition = Vector2(
          position.dx + (size / 2 - attack.size / 2), position.dy + size);
      attack.sprite.current = attack.sprite.down;
    } else if (direction == Direction.left) {
      attackPosition = Vector2(position.dx - attack.size,
          position.dy + (size / 2 - attack.size / 2));
      attack.sprite.current = attack.sprite.left;
    } else {
      attackPosition = Vector2(
          position.dx + size, position.dy + (size / 2 - attack.size / 2));
      attack.sprite.current = attack.sprite.right;
    }
    return attackPosition;
  }
}
