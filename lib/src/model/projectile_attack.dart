part of game;

class ProjectileAttack extends BaseAttack {
  Vector2 direction = Vector2(0.0, 0.0);
  int? randomAttackArcDegrees;

  Random random = Random();

  ProjectileAttack(
      int dmg, int cooldownDuration, double speed, int size, Sprite sprite,
      [this.randomAttackArcDegrees])
      : super(
            -1, dmg, cooldownDuration, speed, size, Vector2(0.0, 0.0), sprite);

  ProjectileAttack.noSprite(
      int dmg, int cooldownDuration, double speed, int size,
      [this.randomAttackArcDegrees])
      : super(-1, dmg, cooldownDuration, speed, size, Vector2(0.0, 0.0),
            Sprite.none());

  ProjectileAttack.clone(ProjectileAttack og, this.direction, Vector2 position)
      : super(getUID(), og.dmg, og.cooldownDuration, og.speed, og.size,
            position, og.sprite) {
    if (og.randomAttackArcDegrees != null) {
      double deviation = random.nextInt(og.randomAttackArcDegrees!) -
          og.randomAttackArcDegrees! / 2;
      direction = direction.rotateDegrees(deviation);
    }
  }

  ProjectileAttack clone(Vector2 direction, Vector2 position) {
    return ProjectileAttack.clone(this, direction, position);
  }

  void update() {
    position = direction * speed + position;
  }
}
