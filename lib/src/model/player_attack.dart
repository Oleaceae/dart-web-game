part of game;

class PlayerAttack extends BaseAttack {
  Direction direction = Direction.up;

  int durationTimer = 0;
  int duration = 25;

  // Speed variable only affects the rotation speed of the css animation

  PlayerAttack(
      int dmg, int cooldownDuration, double speed, int size, Sprite sprite)
      : super(
            -1, dmg, cooldownDuration, speed, size, Vector2(0.0, 0.0), sprite) {
    this.cooldown = this.cooldownDuration;
  }

  PlayerAttack.clone(PlayerAttack og, this.direction, Vector2 position)
      : super(getUID(), og.dmg, og.cooldownDuration, og.speed, og.size,
            Vector2(0.0, 0.0), og.sprite) {
    this.position = position;
  }

  PlayerAttack.simple()
      : super(
            -1,
            1,
            20,
            2.3,
            32,
            Vector2(0, 0),
            Sprite(
                'assets/pc/up.png',
                'assets/pc/down.png',
                'assets/pc/right.png',
                'assets/pc/left.png',
                'assets/pc/up.png'));

  bool isReady() {
    return cooldown == cooldownDuration;
  }

  bool ended() {
    return durationTimer == duration;
  }

  bool onCoolDown() {
    return cooldown < cooldownDuration;
  }
}
