part of game;

class Exit extends Collider {
  String linkedLevelID;

  // Placement of PC on new level
  Vector2 exitPosition;

  bool open = true;
  String? opensWhen;
  Sprite? openSprite;

  Exit.noSprite(
      this.linkedLevelID, this.exitPosition, int id, Vector2 position, int size)
      : super(id, position, size, Sprite.none());

  Exit(this.linkedLevelID, this.exitPosition, int id, Vector2 position,
      int size, Sprite sprite, this.openSprite, this.opensWhen, this.open)
      : super(id, position, size, sprite);
}
