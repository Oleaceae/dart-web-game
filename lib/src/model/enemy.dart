part of game;

class Enemy extends Collider {
  Random random = Random();
  int dmg = 1;
  int maxHp = 1;
  int currentHp = 1;

  double moveSpeed = 2.0;

  int movementFrames = 0;
  int movementDirection = 0;

  bool invulnerability = false;
  int invulnerabilityDuration = 60;

  List<ProjectileAttack> baseAttacks = [];

  Enemy(this.dmg, int id, Vector2 position, int size, Sprite sprite, this.maxHp,
      this.moveSpeed)
      : super(id, position, size, sprite) {
    this.currentHp = this.maxHp;
  }

  Enemy.noSprite(this.dmg, int id, Vector2 position, int size)
      : super(id, position, size, Sprite.none());

  Enemy.error() : super(-1, Vector2(0.0, 0.0), 0, Sprite.none());

  void damaged(int damage) {
    if (currentHp > 0) {
      currentHp -= damage;
      eventQueue.add(
          Event.enemyDamaged('enemyDamaged', this, invulnerabilityDuration));
    }
    if (currentHp <= 0) {
      currentHp = 0;
      eventQueue.add(Event.enemyDeath('enemyDeath', this));
    }
  }

  void randomMovement(GameField field) {
    if (movementFrames == 0) {
      if (random.nextInt(30) == 0) {
        movementFrames = 30 + random.nextInt(90);
        movementDirection = random.nextInt(4);
      }
      return;
    }

    movementFrames -= 1;

    double dx = 0;
    double dy = 0;

    String current = sprite.current;

    if (movementDirection == 0) {
      dy = -1;
      sprite.current = sprite.up;
    } else if (movementDirection == 1) {
      dy = 1;
      sprite.current = sprite.down;
    } else if (movementDirection == 2) {
      dx = 1;
      sprite.current = sprite.right;
    } else {
      dx = -1;
      sprite.current = sprite.left;
    }

    if (sprite.current != current) {
      eventQueue.add(Event.updateEnemySprite('updateEnemy', this));
    }

    Collider projectedLocation =
        Collider.noSprite(0, position + Vector2(dx, dy) * moveSpeed, size);

    for (Collider collider in field.colliders) {
      if (projectedLocation.collidesWith(collider)) return;
    }

    for (Enemy enemy in field.enemies) {
      if (enemy.id != id && projectedLocation.collidesWith(enemy)) return;
    }

    if (projectedLocation.outOfBounds(field)) return;

    position = position + Vector2(dx, dy) * moveSpeed;
  }

  void attack(GameField field) {
    for (ProjectileAttack attack in baseAttacks) {
      if (attack.cooldown >= attack.cooldownDuration) {
        field.activeAttacks.add(attack.clone(distanceVectorToPlayer(field),
            position + Vector2.single(size / 2)));
        attack.cooldown = 0;
      }
      attack.cooldown += 1;
    }
  }

  bool isInvulnerable() {
    return invulnerability;
  }

  Vector2 distanceVectorToPlayer(GameField field) {
    return (field.pc.position - position).normalize();
  }
}
