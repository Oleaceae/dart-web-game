part of game;

class SpriteObject extends GenericObject {
  Sprite sprite = Sprite.none();

  SpriteObject(int id, Vector2 position, int size, this.sprite)
      : super(id, position, size);

  SpriteObject.noSprite(int id, Vector2 position, int size)
      : super(id, position, size);
}
