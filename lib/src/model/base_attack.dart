part of game;

class BaseAttack extends Collider {
  int dmg = 1;
  bool active = false;
  double speed = 1.0;

  int cooldown = 0;
  int cooldownDuration = 120;

  BaseAttack(int id, this.dmg, this.cooldownDuration, this.speed, int size,
      Vector2 position, Sprite sprite)
      : super(id, position, size, sprite);
}
