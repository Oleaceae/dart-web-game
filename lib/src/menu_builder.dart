part of game;

class MenuBuilder {
  late DOMManipulator dom;

  MenuBuilder(this.dom);

  final hsEntryMenuWidth = 250;
  final hsEntryMenuHeight = 200;
  final hsEntryMenuMarginTop = 200;

  final int hsMenuWidth = 500;
  final int hsMenuHeight = 700;
  final int hsMenuMarginTop = 50;

  final String backgroundImage = 'assets/background/highscores-menu.png';

  Map<String, Element> createHighscoreEnterMenu(
      Duration highscoreTime, String eventID) {
    DivElement highscoreBox = DivElement();
    highscoreBox.style.position = 'relative';
    highscoreBox.style.width = '${hsEntryMenuWidth * dom.sizeFactor}px';
    highscoreBox.style.height = '${hsEntryMenuHeight * dom.sizeFactor}px';
    highscoreBox.style.border = 'thin solid black';
    highscoreBox.style.margin = 'auto';
    highscoreBox.style.top = '${hsEntryMenuMarginTop * dom.sizeFactor}px';
    highscoreBox.style.backgroundImage = 'url(${backgroundImage})';
    highscoreBox.style.backgroundSize = '100% 100%';
    highscoreBox.className = eventID;

    ParagraphElement header = ParagraphElement();
    header.innerText = 'New Highscore!';
    header.style.marginTop = '${20 * dom.sizeFactor}px';
    header.style.marginBottom = '0px';
    header.style.whiteSpace = 'nowrap';
    header.style.color = 'white';

    TextInputElement nameField = TextInputElement();
    nameField.placeholder = 'Name';

    PasswordInputElement passwordField = PasswordInputElement();
    passwordField.placeholder = 'Password for deletion';

    ParagraphElement time = ParagraphElement();
    time.innerText = _formatDuration(highscoreTime);
    time.style.color = 'white';

    ButtonElement accept = ButtonElement();
    accept.innerText = 'Accept';
    ButtonElement cancel = ButtonElement();
    cancel.innerText = 'Cancel';

    List<Element> menuElements = [
      header,
      nameField,
      passwordField,
      time,
      accept,
      cancel
    ];
    menuElements.forEach((element) {
      element.className = eventID;
      element.style.position = 'absolute';
      element.style.zIndex = '1000';
      element.style.fontSize = '${16 * dom.sizeFactor}px';
    });

    [header, nameField, passwordField, time].forEach((element) {
      element.style.left = '50%';
      element.style.marginRight = '50%';
      element.style.transform = 'translate(-50%, -50%)';
    });

    accept.style.left = '20%';
    cancel.style.left = '60%';

    int elementSpacing = (50 * dom.sizeFactor).toInt();
    [nameField, passwordField, time].forEach((element) {
      element.style.top = '${elementSpacing}px';
      elementSpacing += (30 * dom.sizeFactor).toInt();
    });
    elementSpacing += (15 * dom.sizeFactor).toInt();

    [accept, cancel].forEach((element) {
      element.style.top = '${elementSpacing}px';
    });
    highscoreBox.children.addAll(menuElements);
    dom.events.children.add(highscoreBox);
    return {
      "header": header,
      "accept": accept,
      "cancel": cancel,
      "name": nameField,
      "password": passwordField,
    };
  }

  String _formatDuration(Duration duration) {
    return duration.toString().split('.').first.padLeft(8, '0');
  }

  void displayMessage(String errorMessage, Element errorDisplay, String color) {
    errorDisplay.innerText = errorMessage;
    errorDisplay.style.color = color;
  }

  Map<String, dynamic> createHighscoreMenu(String eventID) {
    DivElement highscoreMenuBox = DivElement();
    highscoreMenuBox.style.position = 'relative';
    highscoreMenuBox.style.display = 'grid';
    highscoreMenuBox.style.width = '${hsMenuWidth * dom.sizeFactor}px';
    highscoreMenuBox.style.height = '${hsMenuHeight * dom.sizeFactor}px';
    highscoreMenuBox.style.margin = 'auto';
    highscoreMenuBox.style.top = '${hsMenuMarginTop * dom.sizeFactor}px';
    highscoreMenuBox.style.zIndex = '1000';
    highscoreMenuBox.style.backgroundImage = 'url(${backgroundImage})';
    highscoreMenuBox.style.backgroundSize = '100% 100%';
    highscoreMenuBox.className = eventID;

    ParagraphElement listHeader = ParagraphElement();
    listHeader.innerText = 'Highscores';
    listHeader.style.fontSize = '${16 * dom.sizeFactor}px';
    listHeader.style.marginTop = '${20 * dom.sizeFactor}px';
    listHeader.style.marginBottom = '0px';
    listHeader.style.whiteSpace = 'nowrap';
    listHeader.style.color = 'white';

    DivElement highscoresBox = DivElement();
    highscoresBox.id = 'HighscoresBox';
    highscoresBox.style.width = '80%';
    highscoresBox.style.marginTop = '${60 * dom.sizeFactor}px';
    highscoresBox.style.marginLeft = '${30 * dom.sizeFactor}px';

    Element orderedList = Element.ol();
    orderedList.style.listStyleType = 'none';
    highscoresBox.children.add(orderedList);

    ButtonElement pageLeft = ButtonElement();
    ImageElement pageLeftIcon = ImageElement(src: 'assets/icons/page-left.png');
    pageLeft.style.marginRight = '${20 * dom.sizeFactor}px';

    ButtonElement pageRight = ButtonElement();
    ImageElement pageRightIcon =
        ImageElement(src: 'assets/icons/page-right.png');

    [pageRightIcon, pageLeftIcon].forEach((element) {
      element.style.height = '${16 * dom.sizeFactor}px';
      element.style.width = '${16 * dom.sizeFactor}px';
    });

    pageRight.children.add(pageRightIcon);
    pageLeft.children.add(pageLeftIcon);

    DivElement pageNavigation = DivElement();
    pageNavigation.id = 'Buttons';
    pageNavigation.style.top = '${470 * dom.sizeFactor}px';
    pageNavigation.style.whiteSpace = 'nowrap';
    pageNavigation.children.addAll([pageLeft, pageRight]);

    ParagraphElement deletionHeader = ParagraphElement();
    deletionHeader.innerText =
        'Delete all highscores of a name & password combination';
    deletionHeader.style.fontSize = '${16 * dom.sizeFactor}px';
    deletionHeader.style.whiteSpace = 'nowrap';
    deletionHeader.style.top = '${500 * dom.sizeFactor}px';
    deletionHeader.style.color = 'white';

    TextInputElement nameField = TextInputElement();
    nameField.placeholder = 'Name';
    nameField.style.fontSize = '${16 * dom.sizeFactor}px';
    nameField.style.width = '${200 * dom.sizeFactor}px';
    nameField.style.height = '${15 * dom.sizeFactor}px';
    nameField.style.marginRight = '${30 * dom.sizeFactor}px';

    PasswordInputElement passwordField = PasswordInputElement();
    passwordField.placeholder = 'Password';
    passwordField.style.fontSize = '${16 * dom.sizeFactor}px';
    passwordField.style.width = '${200 * dom.sizeFactor}px';
    passwordField.style.height = '${20 * dom.sizeFactor}px';

    DivElement inputBox = DivElement();
    inputBox.style.whiteSpace = 'nowrap';
    inputBox.style.top = '${550 * dom.sizeFactor}px';

    inputBox.children.addAll([nameField, passwordField]);

    ButtonElement delete = ButtonElement();
    delete.innerText = 'Delete';
    delete.style.fontSize = '${14 * dom.sizeFactor}px';
    delete.style.marginRight = '${30 * dom.sizeFactor}px';
    ButtonElement cancel = ButtonElement();
    cancel.style.fontSize = '${14 * dom.sizeFactor}px';
    cancel.innerText = 'Cancel';

    DivElement buttonBox = DivElement();
    buttonBox.id = 'Buttons';
    buttonBox.style.top = '${600 * dom.sizeFactor}px';
    buttonBox.style.whiteSpace = 'nowrap';
    buttonBox.children.addAll([delete, cancel]);

    [listHeader, deletionHeader, pageNavigation, inputBox, buttonBox]
        .forEach((element) {
      element.style.left = '50%';
      element.style.marginRight = '50%';
      element.style.transform = 'translate(-50%, -50%)';
    });

    List<Element> menuItems = [
      listHeader,
      highscoresBox,
      pageNavigation,
      deletionHeader,
      inputBox,
      buttonBox,
    ];

    menuItems.forEach((element) {
      element.className = eventID;
      element.style.position = 'absolute';
    });

    highscoreMenuBox.children.addAll(menuItems);
    dom.events.children.add(highscoreMenuBox);

    return {
      "header": listHeader,
      "name": nameField,
      "password": passwordField,
      "pageLeft": pageLeft,
      "pageRight": pageRight,
      "delete": delete,
      "cancel": cancel,
      "highscoresBox": highscoresBox
    };
  }

  void addHighscores(Element highscoresBox, dynamic highscores, int offset) {
    // List item numbering: Offset starts at 0, first element should be 1.
    int count = offset + 1;
    for (dynamic highscore in highscores) {
      Element listItem = Element.li();
      listItem.style.height = '${18 * dom.sizeFactor}px';

      Element counter = ParagraphElement();
      counter.innerText = count.toString().padLeft(2, '0') + '.';
      count += 1;

      Element name = ParagraphElement();
      name.innerText = highscore['name'];
      name.style.whiteSpace = 'nowrap';
      name.style.display = 'block';
      name.style.overflow = 'hidden';
      name.style.textOverflow = 'ellipsis';
      name.style.marginLeft = '${30 * dom.sizeFactor}px';
      name.style.width = '${230 * dom.sizeFactor}px';

      Element time = ParagraphElement();
      time.innerText = _formatDuration(Duration(seconds: highscore['time']));
      time.style.left = '80%';

      List<Element> highscoreElements = [counter, name, time];
      highscoreElements.forEach((element) {
        element.style.fontSize = '${16 * dom.sizeFactor}px';
        element.style.position = 'absolute';
        element.style.color = 'white';
      });

      listItem.children.addAll(highscoreElements);

      highscoresBox.children.first.children.add(listItem);
    }
  }

  void removeHighscores(Element highscoresBox) {
    highscoresBox.children.first.children = [];
  }

  void disableButton(ButtonElement button, bool disabled) {
    button.disabled = disabled;
  }
}
