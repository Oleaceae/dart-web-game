part of game;

/// Singleton queue for pumping of events
/// Processed once at the start of every frame
/// See [Event] for implementation of single events
class EventQueue {
  List<Event> events = [];

  bool process(Game controller) {
    // Discard finished events
    events = List.from(events.where((event) => !event.done));
    // Run events & stop further events if any return true (BLOCKING EVENT)
    return events.any((event) => event.run(controller));
  }

  void add(Event) {
    events.add(Event);
  }

  void addUnique(Event other) {
    if (this.contains(other)) return;
    events.add(other);
  }

  void addAll(Iterable<Event> events) {
    this.events.addAll(events);
  }

  bool contains(Event other) {
    for (Event event in events) {
      if (event.eventID == other.eventID) return true;
    }
    return false;
  }
}
