part of game;

class DOMManipulator {
  DivElement screen = DivElement();
  DivElement player = DivElement();
  DivElement gameField = DivElement();
  DivElement ui = DivElement();
  DivElement colliders = DivElement();
  DivElement staticSprites = DivElement();
  DivElement enemies = DivElement();
  DivElement exits = DivElement();
  DivElement lockedExits = DivElement();
  DivElement hearts = DivElement();
  DivElement events = DivElement();
  DivElement texts = DivElement();
  DivElement attacks = DivElement();

  // Variable used for resizing everything if there's not enough space
  // in the window for the entire game field (1080x800)
  double sizeFactor = 1.0;

  late Rectangle windowSize;

  // Space between game field and window border
  double marginLeft = 0.0;
  double marginTop = 0.0;

  late double width;
  late double height;

  DOMManipulator(this.screen, UIController uiController, GameField field) {
    // Get window dimensions
    windowSize = screen.getBoundingClientRect();

    // Get game field dimensions
    width = field.width;
    height = field.height;

    // Scale by factor if windowSize is not large enough to render the field
    if (width > windowSize.width || height > windowSize.height) {
      double widthFactor = windowSize.width / width;
      double heightFactor = windowSize.height / height;

      sizeFactor = widthFactor < heightFactor ? widthFactor : heightFactor;
    }

    width = width * sizeFactor;
    height = height * sizeFactor;

    marginLeft = (windowSize.width - width) / 2;
    marginTop = (windowSize.height - height) / 2;

    // Initialize the game field
    gameField.id = 'gameField';
    screen.children.add(gameField);

    // Initialize the ui
    ui.id = 'ui';
    events.id = 'events';
    ui.children.add(events);
    screen.children.add(ui);

    // Render the game field
    gameField.style.left = '${marginLeft}px';
    gameField.style.top = '${marginTop}px';
    gameField.style.width = '${width}px';
    gameField.style.height = '${height}px';
    gameField.style.position = 'absolute';
    gameField.style.border = 'ridge white';
    gameField.style.backgroundImage = 'url(${field.backgroundImage})';
    gameField.style.backgroundSize = 'contain';

    // Position the UI
    ui.style.left = '${marginLeft}px';
    ui.style.top = '${marginTop}px';
    ui.style.width = '${width}px';
    ui.style.height = '${height}px';
    ui.style.position = 'absolute';

    updateHearts(uiController);
    render(field, uiController);
  }

  DOMManipulator cleanField() {
    player = DivElement();
    colliders = DivElement();
    staticSprites = DivElement();
    enemies = DivElement();
    exits = DivElement();
    lockedExits = DivElement();
    texts = DivElement();
    attacks = DivElement();
    events = DivElement();

    gameField.children = [];

    return this;
  }

  DOMManipulator render(GameField field, [UIController? uiController]) {
    gameField.style.backgroundImage = 'url(${field.backgroundImage})';

    // Add objects
    player.id = 'player';
    colliders.id = 'static-colliders';
    staticSprites.id = 'static-sprites';
    enemies.id = 'enemies';
    exits.id = 'exits';
    lockedExits.id = 'lockedExits';
    texts.id = 'texts';
    attacks.id = 'attacks';
    events.id = 'events';

    gameField.children.add(player);
    gameField.children.add(enemies);
    gameField.children.add(colliders);
    gameField.children.add(staticSprites);
    gameField.children.add(exits);
    gameField.children.add(lockedExits);
    gameField.children.add(texts);
    gameField.children.add(attacks);
    gameField.children.add(events);

    addSpriteObjects([field.pc], player);
    // The player should show up on top of objects
    (player.children.first as ImageElement).style.zIndex = '1';
    addSpriteObjects(field.itemPickups, colliders);
    addSpriteObjects(field.colliders, colliders);
    addSpriteObjects(field.staticSprites, staticSprites);
    addSpriteObjects(field.enemies, enemies);
    addSpriteObjects(field.exits, exits);
    addSpriteObjects(field.lockedExits, lockedExits);
    addTextObjects(field.textObjects, texts);

    enemies.children.forEach((enemy) => enemy.style.zIndex = '2');

    return this;
  }

  DOMManipulator removeCollider(Collider collider) {
    colliders.children.removeWhere((other) {
      return other.id.compareTo('${collider.id}') == 0;
    });
    return this;
  }

  void renderDisplayEvent(DisplayEvent event) {
    addTextObjects(event.textObjects, events, event.eventID);
    addSpriteObjects(event.spriteObjects, events, event.eventID);
  }

  void removeEvent(Event event) {
    events.children.removeWhere((other) {
      return other.className.compareTo(event.eventID) == 0;
    });
  }

  void removeHearts() {
    ui.children.remove(hearts);
  }

  void updateHearts(UIController uiController) {
    removeHearts();

    hearts = DivElement();
    hearts.id = 'hearts';

    ui.children.add(hearts);

    addSpriteObjects(uiController.hearts, hearts);
  }

  void updatePlayerPosition(PlayerCharacter playerCharacter) {
    (player.children.first as ImageElement).style.left =
        '${playerCharacter.position.dx * sizeFactor}px';
    (player.children.first as ImageElement).style.top =
        '${playerCharacter.position.dy * sizeFactor}px';
  }

  void updatePlayerSprite(PlayerCharacter playerCharacter) {
    (player.children.first as ImageElement).src =
        '${playerCharacter.sprite.current}';
  }

  void updatePlayerSize(PlayerCharacter playerCharacter) {
    ImageElement playerSprite = player.children.first as ImageElement;
    playerSprite.width = (playerCharacter.size * sizeFactor).toInt();
    playerSprite.height = (playerCharacter.size * sizeFactor).toInt();
  }

  void updateEnemyPositions(List<Enemy> enemies) {
    // Dirty way of updating each enemy's position while keeping complexity at O(n).
    // Make sure the two lists are of the same length and sorted
    for (int i = 0; i < enemies.length; i++) {
      this.enemies.children[i].style.left =
          '${enemies[i].position.dx * sizeFactor}px';
      this.enemies.children[i].style.top =
          '${enemies[i].position.dy * sizeFactor}px';
    }
  }

  void updateEnemySprite(Enemy enemy) {
    Element enemyElement = enemies.children.firstWhere(
        (other) => '${enemy.id}' == other.id,
        orElse: () => ImageElement());

    // Couldn't find it? Don't update. Happens when switching levels or enemies die
    if (enemyElement.id.isEmpty) return;

    (enemyElement as ImageElement).src = enemy.sprite.current;
  }

  void setPlayerInvulnerabilityAnimation(bool enable) {
    if (enable) {
      player.style.animation = 'flash 1s linear infinite';
    } else {
      player.style.animation = '';
    }
  }

  static DivElement initScreen(Element htmlBody) {
    DivElement root = DivElement();
    root.style.position = 'relative';
    root.style.width = '100%';
    root.style.height = '100%';
    root.style.minHeight = '100%';
    root.style.marginLeft = 'auto';
    root.style.marginRight = 'auto';
    root.style.display = 'inline-block';
    htmlBody.children.add(root);
    return root;
  }

  void addSpriteObjects(List<SpriteObject> objects, DivElement parent,
      [String className = '']) {
    for (SpriteObject obj in objects) {
      ImageElement newObject = ImageElement();
      newObject.id = '${obj.id}';
      if (className.isNotEmpty) {
        newObject.className = className;
      }
      newObject.style.left = '${obj.position.dx * sizeFactor}px';
      newObject.style.top = '${obj.position.dy * sizeFactor}px';
      newObject.src = obj.sprite.current;
      newObject.style.position = 'absolute';
      newObject.width = (obj.size * sizeFactor).toInt();
      newObject.height = (obj.size * sizeFactor).toInt();
      newObject.style.objectFit = 'fill';
      parent.children.add(newObject);
    }
  }

  void addTextObjects(List<TextObject> objects, DivElement parent,
      [String className = '']) {
    for (TextObject obj in objects) {
      ParagraphElement newObject = ParagraphElement();
      newObject.id = '${obj.id}';
      newObject.text = '${obj.text}';
      if (className.isNotEmpty) {
        newObject.className = className;
      }
      newObject.style.left = '${obj.position.dx * sizeFactor}px';
      newObject.style.top = '${obj.position.dy * sizeFactor}px';
      newObject.style.position = 'absolute';
      newObject.style.fontSize = '${obj.size * sizeFactor}px';
      newObject.style.color = '${obj.color}';
      newObject.style.zIndex = '3';
      parent.children.add(newObject);
    }
  }

  // TODO consider making these more generic
  void createAttack(BaseAttack attack, [bool fadeOut = false]) {
    addSpriteObjects([attack], attacks);
    attacks.children.where((element) {
      return element.id == "${attack.id}";
    }).forEach((element) {
      element.style.animation =
          'rotateAttack ${2000 / attack.speed}ms linear infinite';
      if (fadeOut) element.style.animation += ', fadeOut 400ms linear';
    });
  }

  void updateAttack(BaseAttack attack) {
    attacks.children.where((other) {
      return attack.id == int.parse(other.id);
    }).forEach((obj) {
      obj.style.left = '${attack.position.dx * sizeFactor}px';
      obj.style.top = '${attack.position.dy * sizeFactor}px';
    });
  }

  void removeAttack(BaseAttack attack) {
    // TODO for consideration: This removes all occurrences in list,
    //  therefore O(n). May be able to make this more efficient by
    //  wrapping it in a .firstWhere().remove(), if performance becomes a problem
    attacks.children.removeWhere((other) {
      return attack.id == int.parse(other.id);
    });
  }

  void removeEnemy(Enemy enemy) {
    enemies.children.removeWhere((other) {
      return enemy.id == int.parse(other.id);
    });
    if (enemies.children.isEmpty) return;

    // Need to sort the list of enemies. See updateEnemyPositions as to why
    // Children of div elements are in some type of snowflake list and can't be sorted directly.
    // Need to store them as a usable list instead, sort and readd them
    List<Element> unsortedEnemies = enemies.children.toList();
    unsortedEnemies.sort((enemy, other) {
      return enemy.id.compareTo(other.id);
    });
    enemies.children = [];
    enemies.children.addAll(unsortedEnemies);
  }

  void setEnemyInvulnerabilityAnimation(Enemy enemy, bool enable) {
    ImageElement target = enemies.children.firstWhere((other) {
      return enemy.id == int.parse(other.id);
    }, orElse: () => ImageElement()) as ImageElement;

    // Couldn't find it? Don't update. Happens when switching levels or enemies die
    if (target.id.isEmpty) return;

    if (enable) {
      target.style.animation = 'flash 200ms linear infinite';
    } else {
      target.style.animation = '';
    }
  }

  void updateExits(GameField field) {
    gameField.children.removeWhere((element) {
      return element.id == 'exits' || element.id == 'lockedExits';
    });

    lockedExits = DivElement();
    exits = DivElement();

    lockedExits.id = 'lockedExits';
    exits.id = 'exits';

    addSpriteObjects(field.exits, exits);
    addSpriteObjects(field.lockedExits, lockedExits);

    gameField.children.add(exits);
    gameField.children.add(lockedExits);
  }
}
