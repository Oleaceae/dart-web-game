part of game;

class UIController {
  List<SpriteObject> hearts = [];
  final int heartSize = 20;
  final double dx = 8;
  final double dy = 8;

  List<SpriteObject> updateHearts(GameField field) {
    double placement = dx;
    this.hearts = [];

    int hearts = field.pc.currentHp ~/ 2;
    for (int i = 0; i < hearts; i++) {
      this.hearts.add(newHeart(placement, 'full'));
      placement += heartSize;
    }
    if (field.pc.currentHp.isOdd) {
      this.hearts.add(newHeart(placement, 'half'));
      placement += heartSize;
    }
    if (field.pc.currentHp != field.pc.maxHp) {
      int remainingHearts = (field.pc.maxHp - field.pc.currentHp) ~/ 2;
      for (int i = 0; i < remainingHearts; i++) {
        this.hearts.add(newHeart(placement, 'empty'));
        placement += heartSize;
      }
    }
    return this.hearts;
  }

  SpriteObject newHeart(double placement, String subtype) {
    return SpriteObject(-1, Vector2(placement, dy), heartSize,
        Sprite.single('assets/heart-$subtype.png'));
  }
}
