part of game;

class Vector2 {
  double dx;
  double dy;
  double length = 0.0;
  final double degreeToRadians = pi / 180;

  Vector2(this.dx, this.dy);

  Vector2 normalize() {
    length = sqrt(pow(dx, 2.0) + pow(dy, 2.0));

    if (length == 0.0) return Vector2(0.0, 0.0);
    final d = 1.0 / length;
    return Vector2(dx * d, dy * d);
  }

  Vector2 operator *(double mult) {
    return Vector2(dx * mult, dy * mult);
  }

  Vector2 operator +(Vector2 vec) {
    return Vector2(dx + vec.dx, dy + vec.dy);
  }

  Vector2 operator -(Vector2 vec) {
    return Vector2(dx - vec.dx, dy - vec.dy);
  }

  Vector2 rotateDegrees(double degrees) {
    double radian = degrees * degreeToRadians;

    double cosA = cos(radian);
    double sinA = sin(radian);

    return Vector2(
        cosA * this.dx - sinA * this.dy, sinA * this.dx + cosA * this.dy);
  }

  Vector2.single(double num) : this(num, num);
}
