part of game;

const int fps = 60;
EventQueue eventQueue = EventQueue();

Random random = Random();

/// Every [GenericObject] gets a unique ID so the DOM can map the model's
/// objects to currently rendered HTML elements for deletion.
/// These are randomly generated in [getUID] and removed in [deleteUID]
/// Set maximum attempts and value of generated UIDs.
const int uidGenerationAttempts = 100;
const int uidMax = 100000;
HashMap<int, bool> uids = HashMap.from({});

class Game {
  late GameField field;
  late UIController uiController;
  late DOMManipulator view;
  late MenuBuilder menuBuilder;
  late LevelManager levelManager;

  DateTime? highscoreTimerStart;
  Duration? highscoreTime;

  bool eventBlocking = false;

  double pcMoveX = 0.0;
  double pcMoveY = 0.0;

  List<String> keyUp = ['w', 'W', 'ArrowUp'];
  List<String> keyLeft = ['a', 'A', 'ArrowLeft'];
  List<String> keyDown = ['s', 'S', 'ArrowDown'];
  List<String> keyRight = ['d', 'D', 'ArrowRight'];
  List<String> keyAttack = ['k', 'K'];
  String keyMenu = 'Escape';

  Future<void> init() async {
    Element? htmlBody = querySelector('#body');

    if (htmlBody == null) {
      throw Exception('Could not find html document body by id \'body\'.');
    }

    DivElement fullScreen = DOMManipulator.initScreen(htmlBody);

    Logger.info('Initializing game field...');
    field = GameField.empty();

    Logger.info('Initializing level manager...');
    levelManager = await LevelManager().loadGame('assets/levels/', field);

    Logger.info('Initializing ui controller...');
    uiController = UIController();

    Logger.info('Loading first level...');
    field = levelManager.loadFirstLevel(field);

    Logger.info('Initializing player HP display...');
    uiController.hearts = uiController.updateHearts(field);

    Logger.info('Initializing DOM manipulator...');
    view = DOMManipulator(fullScreen, uiController, field);
    view.updateHearts(uiController);

    Logger.info('Initializing menu builder...');
    menuBuilder = MenuBuilder(view);

    Logger.info('Initializing input handler...');
    inputHandler();

    Logger.info('Starting game...');
    gameLoop();
  }

  // TODO loop size keeps increasing. Downsize by refactoring parts into events
  void gameLoop() {
    Timer.periodic(Duration(milliseconds: (1000 ~/ fps)), (gameLoop) async {
      // Process the event queue.
      // If process returns true, there's a blocking event.
      // In this case the game loop should not continue.
      if (eventQueue.process(this)) {
        return;
      }

      // Collision with anything that can damage the player
      handlePlayerDamageCollision();

      // Collision of player attacks and enemies
      playerAttackCollision();

      // Player movement
      handlePlayerMovement();

      // Movement & attacks of enemies
      for (Enemy enemy in field.enemies) {
        enemy.attack(field);
        enemy.randomMovement(field);
      }

      // Create, update and delete enemy attacks
      handleEnemyAttacks();

      // Create, update and delete player attacks
      handlePlayerAttacks();

      view.updatePlayerPosition(field.pc);
      view.updateEnemyPositions(field.enemies);
    });
  }

  void handlePlayerMovement() {
    field = field.pc.move(Vector2(pcMoveX, pcMoveY), field, levelManager);
  }

  void inputHandler() {
    window.onKeyDown.listen((ev) {
      if (keyUp.any((key) => key == ev.key))
        pcMoveY = -1.0;
      else if (keyLeft.any((key) => key == ev.key))
        pcMoveX = -1.0;
      else if (keyDown.any((key) => key == ev.key))
        pcMoveY = 1.0;
      else if (keyRight.any((key) => key == ev.key))
        pcMoveX = 1.0;
      else if (keyAttack.any((key) => key == ev.key) &&
          !field.pc.attack.onCoolDown()) {
        field.pc.addAttack();
      } else if (ev.key == keyMenu) {
        eventQueue.addUnique(Event.highscoreMenuDisplay('HighscoreMenu'));
      }
    });
    window.onKeyUp.listen((ev) {
      if (keyUp.any((key) => key == ev.key))
        pcMoveY = 0.0;
      else if (keyLeft.any((key) => key == ev.key))
        pcMoveX = 0.0;
      else if (keyDown.any((key) => key == ev.key))
        pcMoveY = 0.0;
      else if (keyRight.any((key) => key == ev.key)) pcMoveX = 0.0;
    });
  }

  // TODO This could be handled by events in Enemy.attack() and update()
  void handleEnemyAttacks() {
    List<ProjectileAttack> outOfBoundsAttacks = [];

    for (ProjectileAttack attack in field.activeAttacks) {
      if (!attack.active) {
        attack.active = true;
        view.createAttack(attack);
      }
      attack.update();
      view.updateAttack(attack);

      if (attack.outOfBounds(field)) {
        outOfBoundsAttacks.add(attack);
      }
    }

    // Delete out of bounds attacks
    for (ProjectileAttack attack in outOfBoundsAttacks) {
      deleteUID(attack.id);
      field.activeAttacks.removeWhere((other) {
        return attack.id == other.id;
      });
      view.removeAttack(attack);
    }
  }

  void handlePlayerAttacks() {
    if (field.pc.attack.onCoolDown()) {
      field.pc.attack.cooldown += 1;
    }

    List<PlayerAttack> finishedAttacks = [];

    for (PlayerAttack attack in field.pc.activeAttacks) {
      if (!attack.active) {
        attack.active = true;
        view.createAttack(attack, true);
      }
      view.updateAttack(attack);
      attack.durationTimer += 1;

      if (attack.ended()) {
        finishedAttacks.add(attack);
      }
    }

    for (PlayerAttack attack in finishedAttacks) {
      deleteUID(attack.id);
      field.pc.activeAttacks.removeWhere((other) {
        return attack.id == other.id;
      });
      view.removeAttack(attack);
    }
    field.pc.updateActiveAttacks();
  }

  // Handle collision with damaging colliders
  void handlePlayerDamageCollision() {
    if (field.pc.isInvulnerable()) return;

    for (Enemy enemy in field.enemies) {
      if (field.pc.collidesWith(enemy)) {
        field.pc.damaged(enemy.dmg, uiController, view, field);
        return;
      }
    }
    for (ProjectileAttack attack in field.activeAttacks) {
      if (attack.collidesWith(field.pc)) {
        field.pc.damaged(attack.dmg, uiController, view, field);
        break;
      }
    }
  }

  void playerAttackCollision() {
    // O(n²). Not a problem as currently there's only 1 attack at a time
    for (PlayerAttack attack in field.pc.activeAttacks) {
      for (Enemy enemy in field.enemies) {
        if (enemy.isInvulnerable()) continue;
        if (attack.collidesWith(enemy)) {
          enemy.damaged(attack.dmg);
        }
      }
    }
  }
}

int getUID() {
  for (int i = 0; i < uidGenerationAttempts; i++) {
    int num = random.nextInt(uidMax);
    if (uids.containsKey(num)) {
      continue;
    } else {
      uids[num] = true;
      return num;
    }
  }
  // As long as uids are properly cleaned up and the game doesn't contain
  // thousands of objects at once, this shouldn't throw.
  // There's still a tiny chance, but this will work in most cases.
  // The browser will slow down to an unplayable state before this
  // can't find an unused UID.
  throw Exception('Exceeded maximum number of UID generation attempts');
}

void deleteUID(int uid) {
  uids.remove(uid);
}
