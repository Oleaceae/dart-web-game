part of game;

class LevelManager {
  HashMap<String, GameField> levels = HashMap.from({});
  String firstLevel = 'entrance';

  GameField loadFirstLevel(GameField field) {
    return loadLevel(field, firstLevel);
  }

  GameField loadLevel(GameField fromField, String level, [Exit? exit]) {
    // Remove invulnerability from any remaining enemies. Fixes a bug
    // where if you leave a field, invulnerability state is never reset
    fromField.enemies.forEach((enemy) => enemy.invulnerability = false);

    if (fromField.currentLevel.isNotEmpty) {
      levels[fromField.currentLevel] = GameField.clone(fromField);
    }

    if (levels[level] == null) {
      throw Exception('Level \'$level\' not found!');
    }

    GameField newLevel = levels[level]!;
    newLevel.pc = fromField.pc;

    if (exit != null) {
      newLevel.pc.position =
          Vector2(exit.exitPosition.dx, exit.exitPosition.dy);
    }

    if (newLevel.events.isNotEmpty) {
      eventQueue.addAll(newLevel.events);
      newLevel.events = [];
    }

    return newLevel;
  }

  Future<LevelManager> loadGame(String path, GameField fieldBase) async {
    Map<String, dynamic> levelsJson = {};

    await HttpRequest.getString('${path}levelLoader.json')
        .then((String content) {
      levelsJson = jsonDecode(content);
    });

    // Set the first level to be loaded
    if (levelsJson.containsKey('firstLevel')) {
      firstLevel = levelsJson['firstLevel'];
    }

    // Load the initial player character
    if (!levelsJson.containsKey('initialPlayerData')) {
      // No player data defined? Load a default setup
      fieldBase.pc = PlayerCharacter.simple();
    } else {
      dynamic data = levelsJson['initialPlayerData'];
      fieldBase.pc = PlayerCharacter(
          data['maxHp'],
          data['currentHp'] ?? data['maxHp'],
          -1,
          Vector2(data['x'], data['y']),
          data['size'],
          data['movementSpeed']);

      dynamic attack = data['playerAttack'] ?? null;
      if (attack != null) {
        fieldBase.pc.attack = PlayerAttack(
            attack['dmg'],
            attack['cooldown'],
            attack['speed'],
            attack['size'],
            Sprite.fromPath(attack['spritePath']));
      } else {
        fieldBase.pc.attack = PlayerAttack.simple();
      }
    }

    // Load levels as defined in assets/levels JSON files into map
    if (!levelsJson.containsKey('levels')) {
      throw Exception('levelLoader does not contain \'levels\' object.');
    }

    List<dynamic> levels = levelsJson['levels'];
    await Future.forEach(levels, (level) async {
      this.levels[level] = await _load(path, level, fieldBase);
    });

    return this;
  }

  Collider colliderFromJSON(dynamic collider) {
    return Collider(getUID(), Vector2(collider['x'], collider['y']),
        collider['size'], Sprite.single(collider['sprite']));
  }

  SpriteObject spriteObjectFromJSON(dynamic sprite) {
    // Same as colliders except these don't need a UID
    return SpriteObject(-1, Vector2(sprite['x'], sprite['y']), sprite['size'],
        Sprite.single(sprite['sprite']));
  }

  Future<GameField> _load(String path, String file, GameField fieldBase) async {
    GameField field = GameField.clone(fieldBase);

    field.colliders = [];
    field.staticSprites = [];
    field.enemies = [];
    field.exits = [];
    field.lockedExits = [];
    field.itemPickups = [];
    field.textObjects = [];

    Map<String, dynamic> level = {};

    field.currentLevel = file;

    await HttpRequest.getString('$path$file.json').then((String content) {
      level = jsonDecode(content);
    });

    field.backgroundImage =
        level['backgroundImage'] ?? 'assets/background/cult.png';

    List<dynamic> colliders = level['colliders'] ?? [];

    int pickupCounter =
        0; // Adds unique ID modifier to each pickup, required to uniquely identify them

    for (var collider in colliders) {
      // Regular colliders are added to the list of colliders
      if (collider['type'] == null) {
        field.colliders.add(colliderFromJSON(collider));
        continue;
      }
      // Handle item pickups, which are a special type of collider that go into itemPickups
      EventCollider item =
          EventCollider.fromCollider(colliderFromJSON(collider));
      switch (collider['type']) {
        case 'healthRecovery':
          item.event =
              Event.healthRecovery('healthRecovery${pickupCounter}', item);
          break;
        case 'healthUpgrade':
          item.event =
              Event.healthUpgrade('healthUpgrade${pickupCounter}', item);
          break;
        case 'attackUpgrade':
          item.event =
              Event.attackUpgrade('attackUpgrade${pickupCounter}', item);
        case 'shrinkPlayer':
          item.event = Event.shrinkPlayer(
              'shrinkPlayer${pickupCounter}', item, collider['factor']);
        case 'speedUpgrade':
          item.event = Event.speedUpgrade(
              'speedUpgrade${pickupCounter}', item, collider['factor']);
        default:
          // If the type isn't supported, add it anyway and log to console
          Logger.info(
              'Item pickup of type ${collider['type']} does not exist.');
          field.itemPickups.add(item);
      }
      field.itemPickups.add(item);
      pickupCounter = pickupCounter + 1;
    }

    List<dynamic> staticSprites = level['staticSprites'] ?? [];
    for (var sprite in staticSprites) {
      field.staticSprites.add(spriteObjectFromJSON(sprite));
    }

    List<dynamic> enemies = level['enemies'] ?? [];
    for (var enemy in enemies) {
      String spritePath = enemy['spritePath'] ?? '';
      Sprite sprite;

      if (spritePath.isNotEmpty) {
        sprite = Sprite.fromPath(spritePath, current: enemy['currentSprite']);
      } else {
        // For backwards compatibility!
        // TODO consider moving all definitions where applicable to fromPath
        sprite = Sprite(
            enemy['sprite-up'],
            enemy['sprite-down'],
            enemy['sprite-right'],
            enemy['sprite-left'],
            enemy['sprite-initial']);
      }

      Enemy newEnemy = Enemy(
          enemy['dmg'],
          getUID(),
          Vector2(enemy['x'], enemy['y']),
          enemy['size'],
          sprite,
          enemy['maxHp'],
          enemy['moveSpeed']);

      List<dynamic> attacks = enemy['attacks'] ?? [];
      for (var attack in attacks) {
        switch (attack['type']) {
          case 'rotating':
            newEnemy.baseAttacks.add(RotatingAttack(
                attack['dmg'],
                attack['cooldown'],
                attack['speed'],
                attack['size'],
                Sprite.single(attack['sprite'])));
          default:
            newEnemy.baseAttacks.add(ProjectileAttack(
                attack['dmg'],
                attack['cooldown'],
                attack['speed'],
                attack['size'],
                Sprite.single(attack['sprite']),
                attack['randomAttackArcDegrees']));
        }
      }
      field.enemies.add(newEnemy);
    }

    if (field.enemies.isNotEmpty) {
      field.enemies.sort((enemy, other) {
        return enemy.id.compareTo(other.id);
      });
    }

    if (level.containsKey('texts')) {
      List<dynamic> texts = level['texts'];
      for (var text in texts) {
        field.textObjects.add(TextObject(
            text['text'],
            getUID(),
            Vector2(text['x'], text['y']),
            text['fontSize'],
            text['color'] ?? 'black'));
      }
    }

    if (level.containsKey('events')) {
      List<dynamic> events = level['events'];
      for (var event in events) {
        switch (event['type']) {
          case 'highscoreTimerStart':
            field.events.add(Event.highscoreTimerStart());
            break;
          case 'highscoreTimerEnd':
            field.events.add(Event.highscoreTimerEnd('HighscoreInputMenu'));
            break;
          case 'displayEvent':
            if (event['displayEvents'] == null) break;

            List<Event> displayEvents = [];

            for (var displayEvent in event['displayEvents']) {
              List<TextObject> textObjects = getTextObjects(displayEvent);
              List<SpriteObject> spriteObjects = getSpriteObjects(displayEvent);

              displayEvents.add(DisplayEvent(
                  displayEvent['id'],
                  textObjects,
                  spriteObjects,
                  displayEvent['duration'],
                  displayEvent['blocking']));
            }
            field.events.addAll(displayEvents);
            break;
        }
      }
    }

    if (!level.containsKey('exits')) {
      Logger.info('Level \'$file\' does not contain exits!');
      return field;
    }

    List<dynamic> exits = level['exits'];

    for (var exit in exits) {
      Exit newExit = Exit(
          exit['linkedLevelID'],
          Vector2(exit['exitX'], exit['exitY']),
          getUID(),
          Vector2(exit['x'], exit['y']),
          exit['size'],
          Sprite.single(exit['spriteClosed'] ?? exit['sprite']),
          Sprite.single(exit['sprite'] ?? null),
          exit['opensWhen'] ?? null,
          exit['opensWhen'] == null ? true : false);

      if (newExit.open) {
        field.exits.add(newExit);
        continue;
      }
      field.lockedExits.add(newExit);
    }
    return field;
  }

  List<TextObject> getTextObjects(displayEvent) {
    if (displayEvent['textObjects'] == null) return [];

    List<TextObject> textObjects = [];
    for (var text in displayEvent['textObjects']) {
      textObjects.add(TextObject(
          text['text'],
          getUID(),
          Vector2(text['x'], text['y']),
          text['fontSize'],
          text['color'] ?? 'black'));
    }
    return textObjects;
  }

  List<SpriteObject> getSpriteObjects(displayEvent) {
    if (displayEvent['spriteObjects'] == null) return [];

    List<SpriteObject> spriteObjects = [];
    for (var sprite in displayEvent['spriteObjects']) {
      spriteObjects.add(SpriteObject(
          getUID(),
          Vector2(sprite['x'], sprite['y']),
          sprite['size'],
          Sprite.single(sprite['sprite'])));
    }
    return spriteObjects;
  }
}
