part of game;

/// [event]s are a lambda with access to the controller, so the entire game.
/// They may have a duration. After running once, they remain in the queue
/// for a set duration.
/// They are pumped into the global event queue by model classes for the
/// controller to process on the next frame.
/// Once they are cleaned up, an optional [endEvent] may be
/// placed in the queue.
/// The controller has the DOM clean up any finished events by event ID.
/// See [run]
class Event {
  bool blocking = false;
  bool done = false;
  bool running = false;

  Map<String, dynamic> parameters = {};

  String eventID = '';

  int framesRan = 0;
  int duration = 180;

  late bool Function(Game controller) event;
  void Function(Game controller)? endEvent;

  // Infinitely running events have a duration of -1
  final infinite = -1;

  Event(this.eventID, this.blocking) {
    event = (controller) {
      return blocking;
    };
  }

  Event.invulnerability(this.eventID, this.duration) {
    event = (controller) {
      controller.view.setPlayerInvulnerabilityAnimation(true);
      controller.field.pc.setInvulnerable(true);
      running = true;
      return blocking;
    };
    endEvent = (controller) {
      controller.field.pc.setInvulnerable(false);
      controller.view.setPlayerInvulnerabilityAnimation(false);
    };
  }

  Event.enemyDamaged(this.eventID, Enemy enemy, this.duration) {
    event = (controller) {
      controller.field.getEnemy(enemy.id).invulnerability = true;
      controller.view.setEnemyInvulnerabilityAnimation(enemy, true);
      running = true;
      return blocking;
    };
    endEvent = (controller) {
      Enemy target = controller.field.getEnemy(enemy.id);
      // Check if getEnemy returned an error. This happens if enemyDeath fired before the endEvent of enemyDamaged.
      if (target.id == -1) return;
      controller.field.getEnemy(enemy.id).invulnerability = false;
      controller.view.setEnemyInvulnerabilityAnimation(enemy, false);
    };
  }

  Event.enemyDeath(this.eventID, Enemy enemy) {
    duration = 1;
    event = (controller) {
      controller.field.removeEnemy(enemy);
      controller.view.removeEnemy(enemy);

      if (controller.field.enemies.isEmpty) {
        _openLockedExits(controller);
      }

      running = true;
      return blocking;
    };
  }

  void _openLockedExits(Game controller) {
    List<Exit> toRemove = [];

    for (Exit exit in controller.field.lockedExits) {
      if (exit.opensWhen == 'enemiesDead') {
        exit.sprite = exit.openSprite!;
        controller.field.exits.add(exit);
        toRemove.add(exit);
      }
    }
    toRemove.forEach((exit) {
      controller.field.lockedExits.remove(exit);
    });
    controller.view.updateExits(controller.field);
  }

  Event.highscoreTimerStart() {
    duration = 1;
    event = (controller) {
      if (controller.highscoreTimerStart == null) {
        controller.highscoreTimerStart = DateTime.now();
      }
      running = true;
      return blocking;
    };
  }

  // Highscore enter menu whenever a level calls this event
  Event.highscoreTimerEnd(this.eventID) {
    duration = infinite;

    event = (controller) {
      // Default highscore timer of 90 minutes if timer was never started
      if (controller.highscoreTimerStart == null) {
        controller.highscoreTimerStart =
            DateTime.now().subtract(Duration(hours: 1, minutes: 30));
      }

      // First time calling this event - get time
      if (controller.highscoreTime == null) {
        controller.highscoreTime =
            DateTime.now().difference(controller.highscoreTimerStart!);
      }

      // Builds the menu and gets menu elements for listener registration
      Map<String, dynamic> menuElements = controller.menuBuilder
          .createHighscoreEnterMenu(controller.highscoreTime!, eventID);

      menuElements['accept'].onClick.listen((click) {
        controller.menuBuilder.disableButton(menuElements['accept'], true);
        controller.menuBuilder.displayMessage(
            'Sending request...', menuElements['header'], 'yellow');
        String highscore = jsonEncode({
          'name': (menuElements["name"] as TextInputElement).value,
          'password': (menuElements["password"] as TextInputElement).value,
          'time': controller.highscoreTime!.inSeconds,
        });

        HttpRequest.request('/api/highscores',
                method: 'POST',
                sendData: highscore,
                requestHeaders: {'Content-Type': 'application/json'})
            .then((HttpRequest response) {
          controller.menuBuilder
              .displayMessage('Success!', menuElements['header'], 'green');
        }).catchError((error) {
          controller.menuBuilder.disableButton(menuElements['accept'], false);
          controller.menuBuilder
              .displayMessage("Error: ${error}", menuElements["header"], 'red');
        });
      });

      menuElements["cancel"].onClick.listen((click) {
        duration = 1;
      });

      running = true;
      return blocking;
    };
    endEvent = (controller) {
      controller.view.removeEvent(this);
    };
  }

  // Highscore menu as displayed on pressing "Escape"
  Event.highscoreMenuDisplay(this.eventID) {
    duration = infinite;
    blocking = true;
    parameters['offset'] = 0;
    parameters['working'] = false;

    event = (controller) {
      int offset = parameters['offset'];
      running = true;

      Map<String, dynamic> menuElements =
          controller.menuBuilder.createHighscoreMenu(eventID);

      controller.menuBuilder.disableButton(menuElements['pageLeft'], true);

      updateHighscoreList(offset, controller, menuElements);

      (menuElements['pageLeft'] as ButtonElement).onClick.listen((click) {
        if (parameters['offset'] - 20 < 0) {
          return;
        }
        parameters['offset'] = parameters['offset'] - 20;
        int offset = parameters['offset'];
        if (offset == 0) {
          controller.menuBuilder.disableButton(menuElements['pageLeft'], true);
        }
        updateHighscoreList(offset, controller, menuElements);
      });

      (menuElements['pageRight'] as ButtonElement).onClick.listen((click) {
        parameters['offset'] = parameters['offset'] + 20;
        int offset = parameters['offset'];
        controller.menuBuilder.disableButton(menuElements['pageLeft'], false);
        updateHighscoreList(offset, controller, menuElements);
      });

      (menuElements['cancel'] as ButtonElement).onClick.listen((click) {
        duration = 1;
      });

      (menuElements['delete'] as ButtonElement).onClick.listen((click) {
        controller.menuBuilder.disableButton(menuElements['delete'], true);
        controller.menuBuilder.displayMessage(
            'Sending request...', menuElements['header'], 'yellow');
        String user = jsonEncode({
          'name': (menuElements['name'] as TextInputElement).value,
          'password': (menuElements['password'] as TextInputElement).value
        });

        HttpRequest.request('/api/highscores',
                method: 'DELETE',
                sendData: user,
                requestHeaders: {'Content-Type': 'application/json'})
            .then((HttpRequest response) {
          // 2xx responses that aren't 205(update content) should display
          // Their content in header. They contain information on why nothing was deleted
          // Why not 403/404? Because those are interpreted as errors by HttpRequest.
          // 209 is a custom status code
          if (response.status == 200 || response.status == 209) {
            controller.menuBuilder.displayMessage(
                response.responseText ?? 'No content',
                menuElements['header'],
                'red');
          } else {
            controller.menuBuilder
                .displayMessage('Success!', menuElements['header'], 'green');
            updateHighscoreList(parameters['offset'], controller, menuElements);
          }
          controller.menuBuilder.disableButton(menuElements['delete'], false);
        }).catchError((error) {
          controller.menuBuilder
              .displayMessage('Error: ${error}', menuElements['header'], 'red');
        });
      });
      return blocking;
    };

    endEvent = (controller) {
      controller.view.removeEvent(this);
    };
  }

  void updateHighscoreList(
      int offset, Game controller, Map<String, dynamic> menuElements) {
    HttpRequest.request('/api/highscores?offset=${offset}')
        .then((HttpRequest response) {
      controller.menuBuilder.removeHighscores(menuElements['highscoresBox']);
      controller.menuBuilder.addHighscores(menuElements['highscoresBox'],
          jsonDecode(response.responseText ?? '{}'), offset);
    }).catchError((error) {
      controller.menuBuilder
          .displayMessage('Error: ${error}', menuElements['header'], 'red');
    });
  }

  Event.updateEnemySprite(this.eventID, Enemy enemy) {
    duration = 1;

    event = (controller) {
      controller.view.updateEnemySprite(enemy);
      running = true;
      return blocking;
    };
  }

  Event.updatePlayerSprite(this.eventID, PlayerCharacter playerCharacter) {
    duration = 1;

    event = (controller) {
      controller.view.updatePlayerSprite(playerCharacter);
      running = true;
      return blocking;
    };
  }

  Event.healthUpgrade(this.eventID, EventCollider collider) {
    duration = 1;

    event = (controller) {
      controller.field.pc.maxHp += 2;
      controller.field.pc.increaseHp(2);
      controller.field.itemPickups.remove(collider);
      controller.uiController.updateHearts(controller.field);

      controller.view
          .removeCollider(collider)
          .updateHearts(controller.uiController);
      running = true;
      return blocking;
    };
  }

  Event.healthRecovery(this.eventID, EventCollider collider) {
    duration = 1;

    event = (controller) {
      if (controller.field.pc.isFullHp()) {
        running = true;
        return blocking;
      }
      controller.field.pc.increaseHp(2);
      controller.field.itemPickups.remove(collider);
      controller.uiController.updateHearts(controller.field);

      controller.view
          .removeCollider(collider)
          .updateHearts(controller.uiController);
      running = true;
      return blocking;
    };
  }

  Event.shrinkPlayer(this.eventID, EventCollider collider, double factor) {
    duration = 1;

    event = (controller) {
      controller.field.pc.size = (controller.field.pc.size * factor).toInt();
      controller.field.itemPickups.remove(collider);

      controller.view.updatePlayerSize(controller.field.pc);
      controller.view.removeCollider(collider);

      running = true;
      return blocking;
    };
  }

  Event.speedUpgrade(this.eventID, EventCollider collider, double factor) {
    duration = 1;

    event = (controller) {
      controller.field.pc.moveSpeed = controller.field.pc.moveSpeed * factor;
      controller.field.itemPickups.remove(collider);

      controller.view.removeCollider(collider);

      running = true;
      return blocking;
    };
  }

  Event.attackUpgrade(this.eventID, EventCollider collider) {
    duration = 1;

    event = (controller) {
      controller.field.pc.attack.size =
          (controller.field.pc.attack.size * 1.5).toInt();
      controller.field.pc.attack.dmg += 1;
      controller.field.pc.attack.sprite =
          Sprite.fromPath('assets/pc/attackUpgraded/');
      controller.field.itemPickups.remove(collider);

      controller.view.removeCollider(collider);

      running = true;
      return blocking;
    };
  }

  // When a new level has been entered by the player, fully update the view
  Event.levelChanged(GameField fromField, Exit exit) {
    duration = 1;

    event = (controller) {
      running = true;

      controller.field = controller.levelManager
          .loadLevel(fromField, exit.linkedLevelID, exit);

      controller.view.cleanField().render(controller.field);
      return blocking;
    };
  }

  void reset() {
    running = false;
    framesRan = 0;
    done = false;
  }

  bool run(Game controller) {
    if (!running) return event(controller);
    if (duration == infinite) return blocking;

    framesRan += 1;
    if (framesRan >= duration) {
      if (endEvent != null) {
        endEvent!(controller);
      }
      controller.view.removeEvent(this);
      done = true;
    }
    return blocking;
  }
}
