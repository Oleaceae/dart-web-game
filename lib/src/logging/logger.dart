part of game;

class Logger {
  static info(String content) {
    print(_format(content, 'INFO'));
  }

  static String _format(String content, String type) {
    var timestamp = (DateTime.now().millisecondsSinceEpoch / 1000).round();
    return '$type | $timestamp | $content';
  }
}
