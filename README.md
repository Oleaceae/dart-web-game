# Clown's Bad Day

![Clown](web/assets/pc/front.png "The hero")
![Clown](web/assets/pc/front.png "The hero")
![Clown](web/assets/pc/front.png "The hero")

This is the source code for the single-page webgame of the same name.

You can try it out on [GitLab](https://oleaceae.gitlab.io/clowns-bad-day/)!

## Why was this made?

This was created for a college class on web development, API development and deployment with kubernetes.
Only the game itself is being hosted on GitLab, there is no Kubernetes cluster running the database or API.
Source for the python flask API can be found at [API](https://gitlab.com/Oleaceae/clowns-bad-day/-/tree/main/api)
and deployment definition for kubernetes at [deployment-example](https://gitlab.com/Oleaceae/clowns-bad-day/-/tree/main/deployment-example).

The project follows the MVC-Pattern and contains the event queue pattern for messaging.

No canvas element or graphics library is used, everything on the screen is an HTML element moved by javascript.

## Building

You will need [Dart](https://dart.dev/) to compile dart into javascript.
Once you have that, run "dart pub global run webdev serve" in the project root directory. Terminal output will tell you
where your instance of the website is being hosted.
