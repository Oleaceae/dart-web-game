import time
from datetime import datetime


class Logger:
    @classmethod
    def log_error(cls, content):
        print(cls._format("ERROR", content), flush=True)

    @classmethod
    def log_info(cls, content):
        print(cls._format("INFO", content), flush=True)

    @classmethod
    def _format(cls, log_type, content):
        if log_type == "ERROR":
            return f"ERROR   | {cls._time()} | {content}"
        if log_type == "INFO":
            return f"INFO    | {cls._time()} | {content}"
        return f"UNKNOWN | {cls._time()} | {content}"

    @classmethod
    def _time(cls):
        return datetime.fromtimestamp(int(time.time()))
