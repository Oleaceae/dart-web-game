from flask import Flask, jsonify, make_response, request
from flask_cors import CORS
from flask_swagger_ui import get_swaggerui_blueprint
from passlib.hash import sha256_crypt
from sqlalchemy import asc
from sqlalchemy.exc import InterfaceError, OperationalError, ProgrammingError

from database import Database
from logger import Logger
from model.highscore.highscore import Highscore

database = Database()

flask = Flask(__name__)
CORS(flask)

SWAGGER_URL = "/api/docs"
API_URL = "/api/docs/definition"

highscoreEntryLimit = 20


@flask.route("/api", methods=["GET"])
def health_check_api():
    return "Alive!"


@flask.route("/api/db", methods=["GET"])
def health_check_db():
    try:
        database.engine.connect()
    except (InterfaceError, OperationalError, ProgrammingError) as error:
        return f"Error: {error}"
    return "Alive!"


@flask.route("/api/highscores", methods=["GET"])
def get_highscores():
    offset = request.args.get("offset", default=0, type=int)

    if offset < 0:
        offset = 0

    session = database.session()
    rows_highscores = (
        session.query(Highscore.name, Highscore.time_s)
        .order_by(asc(Highscore.time_s))
        .offset(offset)
        .limit(highscoreEntryLimit)
        .all()
    )
    session.close()
    response_json = [
        {
            "name": highscore.name,
            "time": highscore.time_s,
        }
        for highscore in rows_highscores
    ]
    return make_response(jsonify(response_json), 200)


@flask.route("/api/highscores", methods=["POST"])
def post_highscores():
    content = request.json

    name = content.get("name")
    password = content.get("password")
    time = content.get("time")

    new_highscore = Highscore(
        name=name,
        password=sha256_crypt.using().hash(password),
        time_s=time,
    )
    session = database.session()
    session.add(new_highscore)
    session.commit()
    session.close()
    return make_response("Success!", 204)


@flask.route("/api/highscores", methods=["DELETE"])
def delete_highscores():
    content = request.json

    name = content.get("name")
    password = content.get("password")

    session = database.session()
    highscores_by_name = session.query(Highscore).filter(Highscore.name == name).all()

    if not highscores_by_name:
        return make_response(f"No entries found for {name}.", 200)

    deleted = False
    for highscore in highscores_by_name:
        pw_hash = highscore.password
        if sha256_crypt.verify(password, pw_hash):
            deleted = True
            session.delete(highscore)
    session.commit()
    session.close()

    if deleted:
        return make_response("Success!", 205)
    else:
        return make_response("Wrong password.", 209)


###############################################################
# FLASK-SWAGGER-UI - https://pypi.org/project/flask-swagger-ui/
###############################################################


# Serve the api.yaml file
@flask.route("/api/docs/definition")
def send_openapi():
    with open("static/api.yaml") as api:
        return api.read()


# Call factory function to create our blueprint
swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={"app_name": "Highscore API"},  # Swagger UI config overrides
    # oauth_config={  # OAuth config. See https://github.com/swagger-api/swagger-ui#oauth2-configuration .
    #    'clientId': "your-client-id",
    #    'clientSecret': "your-client-secret-if-required",
    #    'realm': "your-realms",
    #    'appName': "your-app-name",
    #    'scopeSeparator': " ",
    #    'additionalQueryStringParams': {'test': "hello"}
    # }
)

flask.register_blueprint(swaggerui_blueprint)

Logger.log_info("Starting highscore API")
flask.run(host="0.0.0.0", port=5000)
