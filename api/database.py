import os
from time import sleep

from sqlalchemy import create_engine, text
from sqlalchemy.exc import InterfaceError, OperationalError, ProgrammingError
from sqlalchemy.orm import sessionmaker

from logger import Logger
from model import Base
from model.highscore import highscore


class Database:
    def __init__(self):
        self.user = os.environ.get("DB_USER", "root")
        self.password = ""
        self.host = os.environ.get("DB_HOST", "mariadb-internal-service")
        self.port = int(os.environ.get("DB_PORT", 3306))
        self.database_name = os.environ.get("DB_DATABASE", "highscore")

        self.engine = create_engine(
            f"mariadb+mariadbconnector://{self.user}:{self.password}@{self.host}:{self.port}/{self.database_name}",
            pool_pre_ping=True,
            pool_size=20,
            max_overflow=10,
        )
        self.session = sessionmaker(self.engine)

        self._check_database_status()

    def _check_database_status(self):
        Logger.log_info("Starting database status check")
        engine = create_engine(
            f"mariadb+mariadbconnector://{self.user}:{self.password}@{self.host}:{self.port}/",
            pool_pre_ping=True,
        )
        try:
            engine.connect()
        except OperationalError:
            Logger.log_error("No connection to MariaDB possible. Attempting to resolve")
            self._fix_database()
        finally:
            Logger.log_info("MariaDB connection OK")

        try:
            self.engine.connect()
        except ProgrammingError as error:
            Logger.log_error(
                f'Database "{self.database_name}" connection could not be established. Attempting to resolve. Error: {error}'
            )
            self._fix_database()
        finally:
            Logger.log_info(f'"{self.database_name}" connection OK')

        Logger.log_info("Checking tables")
        Base.metadata.create_all(bind=self.engine)

    def _fix_database(self):
        engine_base = create_engine(
            f"mariadb+mariadbconnector://{self.user}:{self.password}@{self.host}:{self.port}/"
        )
        session = sessionmaker(engine_base)
        while True:
            try:
                connection = engine_base.connect()
            except (OperationalError, InterfaceError) as error:
                Logger.log_error(
                    f"MariaDB connection could not be established. Check database status and validity of environment variables. Error: {error}"
                )
                sleep(5)
                continue

            Logger.log_info("Connection to MariaDB established. Checking environment")
            break

        engine = create_engine(
            f"mariadb+mariadbconnector://{self.user}:{self.password}@{self.host}:{self.port}/{self.database_name}"
        )

        try:
            connection = engine.connect()
        except ProgrammingError:
            Logger.log_info(f"Database {self.database_name} not found. Creating...")

            try:
                with engine_base.connect() as connection:
                    connection.execute(text(f"CREATE DATABASE `{self.database_name}`"))
                    connection.commit()
                    connection.close()
            except ProgrammingError as error:
                Logger.log_error(
                    f"Unknown error during creation of database.\nError: {error}"
                )
                connection.close()
