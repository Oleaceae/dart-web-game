from sqlalchemy import Column, Integer, String

from .. import Base


class Highscore(Base):
    __tablename__ = "highscore"

    uid = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    time_s = Column(Integer)
    password = Column(String(255))
